class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    setName = (name) => {
        this.name = name;
    }

    getName = () => {
        return this.name;
    }

    setAge = (age) => {
        this.age = age;
    }

    getAge = () => {
        return this.age;
    }

    setSalary = (salary) => {
        this.salary = salary;
    }

    getSalary() {
        return this.salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    getSalary() {
        return super.getSalary() * 3;
    }
}

const programmer1 = new Programmer('Vasya', 18, 1000, ['en', 'ua']);
const programmer2 = new Programmer('Petya', 28, 2000, ['fr', 'en']);
const programmer3 = new Programmer('Kolya', 38, 5000, ['fr', 'en', 'ua']);

console.log(programmer1, programmer3, programmer2);